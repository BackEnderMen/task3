//
//  DetailsViewController.swift
//  task2.1
//
//  Created by Viktor on 01.12.2020.
//  Copyright © 2020 Viktor. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var votedLabel: UILabel!
    
    @IBOutlet weak var circleView: CircleProgressBar!
    private var movie: Movie?
    private let fetcher = MovieStore.shared
    public var idOfMovie: Int?
    public var loadFromCoreData: Bool?
    public var lovedMovie: Movies?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(loadFromCoreData != nil && loadFromCoreData!){
            loadFromCoreData = false
            if(lovedMovie == nil){
                showAlertError(errorMessage: "Loading film when is not internet access failed")
            }
            self.setupWithCoreData(with: lovedMovie!)
        }else{
            fetcher.fetchMovie(id: idOfMovie ?? -1) { (result) in
               switch result {
               case .success(let response):
                self.setup(with: response)
               case .failure(let error):
                
                DispatchQueue.main.async(execute: {
                  let message = "\(error.errorUserInfo.map { $0.1 }.joined(separator: ";\n"))\n Be sure You have access to the Internet.\nTry later!"
                                   
                   self.showAlertError(errorMessage: message)
                })
               }
            }
        }

    }
    
    private func setup(with model: Movie) {
        
        self.titleLabel.text = model.title
        self.yearLabel.text = model.yearText
        do{
            let data = try Data(contentsOf: model.posterURL)
            self.imageView?.image = UIImage(data: data)
        }catch {
          let message = "Failed to load details screen.\n Be sure You have access to the Internet.\nTry later!"
           self.showAlertError(errorMessage: message)
        }
      
        circleView.progressLvl = model.rating
        circleView.animate(toValue: CGFloat( model.rating) / 100)

        self.votedLabel.text = "Проголосували: \(model.voteCount)"
        self.descLabel.text = model.overview
    }
    
    public func setupWithCoreData(with model: Movies) {
        
        self.titleLabel.text = model.title
        self.yearLabel.text = model.yearText
        self.imageView?.image = UIImage(data: model.image)
      
        circleView.progressLvl = model.rating
        circleView.animate(toValue: CGFloat( model.rating) / 100)

        self.votedLabel.text = "Проголосували: \(model.voteCount)"
        self.descLabel.text = model.overview
    }
    
    

    func showAlertError(errorMessage: String)  {
            let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertController.Style.alert)

           alert.addAction(UIAlertAction(title: "Close app", style: .default, handler: { (action: UIAlertAction!) in
               exit(0)
              }))
           
            alert.addAction(UIAlertAction(title: "Refresh", style: .default, handler: { (action: UIAlertAction!) in
               self.viewDidLoad()
              }))
           
          self.present(alert, animated: true, completion: nil)
       }
}
