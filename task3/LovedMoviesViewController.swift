//
//  LovedMoviesViewController.swift
//  task3
//
//  Created by Viktor on 16.12.2020.
//  Copyright © 2020 Viktor. All rights reserved.
//

import UIKit
import CoreData


class LovedMoviesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private let fetcher = MovieStore.shared
    private var lovedMovies = [Movies]()
    let fetchRequest: NSFetchRequest<Movies> = Movies.fetchRequest()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(BigContainerView.nib(), forCellReuseIdentifier: "BigContainerView")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool)  {
        super.viewWillAppear(animated)
         do {
           let movies = try PersistenceServce.context.fetch(fetchRequest)
            self.deleteMovies(newMovies: movies)
            self.loadMovies(newMovies: movies)
           
       } catch {}
          
    }
    
    private func deleteMovies(newMovies:[Movies]){
        var identifier = 0
        
        self.lovedMovies.forEach{elem in
            let exist  = newMovies.firstIndex{$0.id == elem.id}
            
            if(exist==nil){
               let indexPath = IndexPath(row: identifier, section: 0)
                
                tableView.beginUpdates()
                self.lovedMovies.remove(at: identifier)
                tableView.deleteRows(at: [indexPath], with: .automatic)
                tableView.endUpdates()
                view.endEditing(true)
            }else{
                identifier+=1
            }
        }
    }
    
    private func loadMovies(newMovies:[Movies]){
        newMovies.forEach { item in
            let index = self.lovedMovies.firstIndex{$0.id == item.id}

            if(index==nil){
                self.lovedMovies.append(item)
                self.addItem()
            }
        }
    }
    
    
    func addItem(){
        let indexPath = IndexPath(row: self.lovedMovies.count - 1, section: 0)
            tableView.beginUpdates()
            tableView.insertRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        
            view.endEditing(true)
    }
    
    func showAlertError(errorMessage: String)  {
         let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Close app", style: .default, handler: { (action: UIAlertAction!) in
            exit(0)
           }))
        
         alert.addAction(UIAlertAction(title: "Refresh", style: .default, handler: { (action: UIAlertAction!) in
            self.viewDidLoad()
           }))
        
           self.present(alert, animated: true, completion: nil)
    }
}

extension LovedMoviesViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
              let detailsViewController = storyBoard.instantiateViewController(withIdentifier: "detailsViewController") as! DetailsViewController
        detailsViewController.lovedMovie = lovedMovies[indexPath.row]
        detailsViewController.loadFromCoreData = true
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
}

extension LovedMoviesViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return lovedMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "BigContainerView", for: indexPath) as! BigContainerView
          
            cell.setBtnProps(btnPressed: true)

            cell.setupWithCoreData(with: lovedMovies[indexPath.row])
            
            cell.callback = { (pressed) -> Void in
                self.onBtnPress(pressed: pressed, movie: self.lovedMovies[indexPath.row])
            }
          return cell
    }
    
    func onBtnPress(pressed: Bool, movie: Movies){
            let index = lovedMovies.firstIndex{$0.id == movie.id} ?? nil
            if(pressed){
                if((index) == nil){
                   let newMovie = Movies(context: PersistenceServce.context)
                    newMovie.id = movie.id
                    newMovie.title = movie.title
                    newMovie.overview = movie.overview
                    newMovie.rating = movie.rating
                    newMovie.voteCount = movie.voteCount
                    newMovie.yearText = movie.yearText
                    newMovie.image = movie.image
                   
                    PersistenceServce.saveContext()
                    
                    self.lovedMovies.append(newMovie)
                }
            }else {
                if((index) != nil){
                    PersistenceServce.context.delete(lovedMovies[index!])
                    PersistenceServce.saveContext()
                }
            }
        }
}
