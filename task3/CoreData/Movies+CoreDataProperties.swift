

import Foundation
import CoreData
import UIKit


extension Movies {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movies> {
        return NSFetchRequest<Movies>(entityName: "Movies")
    }

    @NSManaged public var id: Int
    @NSManaged public var title: String
    @NSManaged public var image: Data
    @NSManaged public var overview: String
    @NSManaged public var rating: Int
    @NSManaged public var voteCount: Int
    @NSManaged public var yearText: String

}

