//
//  BigCollectionViewCell.swift
//  task3
//
//  Created by Viktor on 23.12.2020.
//  Copyright © 2020 Viktor. All rights reserved.
//

import UIKit

class BigCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var circleBar: CircleProgressBar!
    @IBOutlet weak var year: UILabel!
    
    @IBOutlet weak var button: UIButton!
    private var btnPressed = false

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    static func nib() -> UINib {
       return UINib(nibName: "BigCollectionViewCell", bundle: nil)
    }
       
   var callback: ((_ pressed: Bool) -> Void)?
       
    @IBAction func onClick(_ sender: UIButton) {
        if(btnPressed){
           sender.setImage(UIImage(systemName: "heart"), for: .normal)
           btnPressed = false;
       }else{
           btnPressed = true;
           sender.setImage(UIImage(systemName: "heart.fill"), for: .normal)
       }
       callback?(btnPressed)
    }
           
       func setBtnProps(btnPressed: Bool){
           self.btnPressed = btnPressed
           if(btnPressed){
               button.setImage(UIImage(systemName: "heart.fill"), for: .normal)
           }else{
               button.setImage(UIImage(systemName: "heart"), for: .normal)

           }
       }
       
    func setup(with model: Movie) {
           self.title.text = model.title
           self.year.text = model.yearText
           self.circleBar.progressLvl = model.rating
           
           do{
               let data = try Data(contentsOf: model.posterURL)
               self.image?.image = UIImage(data: data)
           }catch{
              print("error with BigCollectionViewCell")
           }
       }
    
}
