//
//  BigContainerView.swift
//  task3
//
//  Created by Viktor on 09.12.2020.
//  Copyright © 2020 Viktor. All rights reserved.
//

import UIKit

class BigContainerView: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var progressBar: CircleProgressBar!
    
    @IBOutlet weak var ImageContainer: UIImageView!
    
    @IBOutlet weak var button: UIButton!
    private var btnPressed = false
    
    override func awakeFromNib() {
              super.awakeFromNib()
          }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
            return UINib(nibName: "BigContainerView", bundle: nil)
        }
    
    var callback: ((_ pressed: Bool) -> Void)?
    
    @IBAction func onBtnClick(_ sender: UIButton) {
        if(btnPressed){
            sender.setImage(UIImage(systemName: "heart"), for: .normal)
            btnPressed = false;
        }else{
            btnPressed = true;
            sender.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        }
        callback?(btnPressed)
    }
    
    func setBtnProps(btnPressed: Bool){
        self.btnPressed = btnPressed
        if(btnPressed){
            button.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        }else{
            button.setImage(UIImage(systemName: "heart"), for: .normal)
        }
    }
    
    func setup(with model: Movie) {
        self.title.text = model.title
        self.year.text = model.yearText
        self.progressBar.progressLvl = model.rating
        
        do{
            let data = try Data(contentsOf: model.posterURL)
            self.ImageContainer?.image = UIImage(data: data)
        }catch{
           print("error with BigContainerView")
        }
    }
    
    func setupWithCoreData(with model: Movies){
            self.title.text = model.title
            self.year.text = model.yearText
            self.progressBar.progressLvl = model.rating
              
            self.ImageContainer?.image = UIImage(data: model.image)
        }
}
