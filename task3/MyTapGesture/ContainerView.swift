//
//  ViewController.swift
//  task3
//
//  Created by Viktor on 08.12.2020.
//  Copyright © 2020 Viktor. All rights reserved.
//


import UIKit

class ContainerView: UIView {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!
    
    private var btnPressed = false

    var callback: ((_ pressed: Bool) -> Void)?

    @IBAction func onClick(_ sender: UIButton) {
        if(btnPressed){
                   sender.setImage(UIImage(systemName: "heart"), for: .normal)
                   btnPressed = false;
               }else{
                   btnPressed = true;
                   sender.setImage(UIImage(systemName: "heart.fill"), for: .normal)
               }
               callback?(btnPressed)
    }
    func setBtnProps(btnPressed: Bool){
        self.btnPressed = btnPressed
        if(btnPressed){
            button.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        }else{
            button.setImage(UIImage(systemName: "heart"), for: .normal)

        }
    }
}
