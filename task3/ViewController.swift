//
//  ViewController.swift
//  task3
//
//  Created by Viktor on 08.12.2020.
//  Copyright © 2020 Viktor. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var topRatedColectionView: UICollectionView!
    @IBOutlet weak var popularColectionView: UICollectionView!
    
    private var page_index = 1;
    private let totalItems  = 999
    private var isLoadingPopular = false
    
    private let fetcher = MovieStore.shared
    private var topRatedMovies = [Movie]()
    private var popularMovies = [Movie]()
    
    private var moviesOBJ = [Movies]()

    private let fetchRequest: NSFetchRequest<Movies> = Movies.fetchRequest()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.popularColectionView.register(BigCollectionViewCell.nib(), forCellWithReuseIdentifier: "BigCollectionViewCell")
        self.popularColectionView.delegate = self
        self.popularColectionView.dataSource = self
        
        self.topRatedColectionView.register(SmallCollectionViewCell.nib(), forCellWithReuseIdentifier: "SmallCollectionViewCell")
        self.topRatedColectionView.delegate = self
        self.topRatedColectionView.dataSource = self
        
        fetchTopRated()
        fetchPopular()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        do {
            let movies = try PersistenceServce.context.fetch(fetchRequest)
            self.moviesOBJ = movies
           } catch {}
        
        if let indexPaths = self.popularColectionView?.indexPathsForVisibleItems {
            self.popularColectionView?.reloadItems(at: indexPaths)
        }
        if let indexPaths = self.topRatedColectionView?.indexPathsForVisibleItems {
            self.topRatedColectionView?.reloadItems(at: indexPaths)
        }
    }
    
    private func fetchTopRated(){
        DispatchQueue.global(qos: .userInitiated).async {
            self.fetcher.fetchMovies(from: .topRated) { (result) in
                switch result {
                case .success(let response):
                    self.addItemsToTopRated(movies: response.results)
                case .failure(let error):
                    DispatchQueue.main.async(execute: {
                         let message = "\(error.errorUserInfo.map { $0.1 }.joined(separator: ";\n"))\n Be sure You have access to the Internet.\nTry later!"
                        self.showAlertError(errorMessage: message)
                    })
                }
            }
        }
    }
    
    private func fetchPopular(){
        DispatchQueue.global(qos: .userInitiated).async {
            self.fetcher.fetchMovies(from: .popular, page_index: self.page_index){(result) in
                switch result {
                case .success(let response):
                    self.page_index += 1

                    DispatchQueue.main.async(execute: {
                       self.addRowsToPopular(movies: response.results)
                        self.isLoadingPopular = false
                    })
                case .failure(let error):
                     DispatchQueue.main.async(execute: {
                        let message = "\(error.errorUserInfo.map { $0.1 }.joined(separator: ";\n"))\n Be sure You have access to the Internet.\nTry later!"
                                        
                        self.showAlertError(errorMessage: message)
                     })
                }
            }
        }
    }
    
    private func addItemsToTopRated(movies: [Movie]){
        self.topRatedMovies = movies
        self.topRatedColectionView.reloadData()
    }
    
    private func addRowsToPopular(movies: [Movie]){
        let beforeCount = popularMovies.count
        self.popularMovies.append(contentsOf: movies)
        let afterCount = popularMovies.count

        let indexPaths = Array(beforeCount...afterCount - 1).map { IndexPath(item: $0, section: 0) }
        popularColectionView.insertItems(at: indexPaths)
    }
    
    func showAlertError(errorMessage: String)  {
         let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true)
        }))
        
         alert.addAction(UIAlertAction(title: "Refresh", style: .default, handler: { (action: UIAlertAction!) in
            self.viewDidLoad()
           }))
        
           self.present(alert, animated: true, completion: nil)
    }
    
    func onBtnPress(pressed: Bool, movie: Movie){
        let index = moviesOBJ.firstIndex{$0.id == movie.id} ?? nil
        if(pressed){
            if((index) == nil){
               let newMovie = Movies(context: PersistenceServce.context)
                newMovie.id = movie.id
                newMovie.title = movie.title
                newMovie.overview = movie.overview
                newMovie.rating = movie.rating
                newMovie.voteCount = movie.voteCount
                newMovie.yearText = movie.yearText
                if(movie.posterPath != nil){
                    let data = try! Data(contentsOf: movie.posterURL)
                    newMovie.image = data
                }
               
                PersistenceServce.saveContext()
                
                self.moviesOBJ.append(newMovie)
            }
        }else {
            if((index) != nil){
                PersistenceServce.context.delete(moviesOBJ[index!])
                PersistenceServce.saveContext()
                
                moviesOBJ.remove(at: index!)
            }
        }
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        if(collectionView==self.popularColectionView){
             return CGSize(width: self.popularColectionView.frame.size.width, height: self.popularColectionView.frame.size.width)
        }else{
             return CGSize(width: self.topRatedColectionView.frame.size.height, height: self.topRatedColectionView.frame.size.height)
        }
    }
}

extension ViewController:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let detailsViewController = storyBoard.instantiateViewController(withIdentifier: "detailsViewController") as! DetailsViewController
        if(collectionView == self.popularColectionView){
            detailsViewController.idOfMovie = popularMovies[indexPath.item].id
        }else{
            detailsViewController.idOfMovie = self.topRatedMovies[indexPath.item].id
        }
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
}

extension ViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView==self.popularColectionView){
            return self.popularMovies.count
        }else{
            return self.topRatedMovies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == self.popularColectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BigCollectionViewCell", for: indexPath) as! BigCollectionViewCell
                    cell.setup(with: popularMovies[indexPath.row])

            let index = moviesOBJ.firstIndex{$0.id == popularMovies[indexPath.row].id} ?? nil
            if((index) != nil){
                cell.setBtnProps(btnPressed: true)
            }else{
                cell.setBtnProps(btnPressed: false)
            }
                    
            cell.callback = { (pressed) -> Void in
                self.onBtnPress(pressed: pressed, movie: self.popularMovies[indexPath.row])
            }
                
            if indexPath.row == popularMovies.count - 1 {
                if self.totalItems > popularMovies.count && !self.isLoadingPopular {
                    self.isLoadingPopular = true
                    self.fetchPopular()
                }
            }
          return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SmallCollectionViewCell", for: indexPath) as! SmallCollectionViewCell
                cell.setup(with: self.topRatedMovies[indexPath.row])

                cell.layer.borderColor = UIColor.black.cgColor
                cell.layer.borderWidth = 1
            
                let index = moviesOBJ.firstIndex{$0.id == self.topRatedMovies[indexPath.row].id} ?? nil
                  if((index) != nil){
                      cell.setBtnProps(btnPressed: true)
                  }else{
                      cell.setBtnProps(btnPressed: false)
                  }
                   
                   cell.callback = { (pressed) -> Void in
                    self.onBtnPress(pressed: pressed, movie: self.topRatedMovies[indexPath.row])
                   }
                 return cell
        }
    }
}

